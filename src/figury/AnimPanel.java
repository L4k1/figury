package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	public Graphics2D buffer;

	private int delay = 30;
	Timer timer;
	private static int numer = 0;
	LinkedList<Figura> figuraList = new LinkedList<>();
	Figura figura;
	
	public int width;
	public int height;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}
	

	public void initialize() {
		width = getWidth();
		height = getHeight();
        
		image = createImage(width, height);
		
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		buffer.setBackground(Color.WHITE);
	}

	void addFig() {
		numer++;
		if (numer % 3 == 0) figura = new Kwadrat(buffer, delay, getWidth(), getHeight());
		if (numer % 3 == 1) figura = new Elipsa(buffer, delay, getWidth(), getHeight());
		if (numer % 3 == 2) figura = new Trojkat(buffer, delay, getWidth(), getHeight());
		
		figuraList.add(figura);
		timer.addActionListener(figura);
		new Thread(figura).start();
	}

	void animate() {

		if (timer.isRunning()) {
			timer.stop();
		} else {
			timer.start();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
	
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
}
