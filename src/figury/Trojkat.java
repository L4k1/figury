package figury;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Trojkat extends Figura {

	public Trojkat(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		
		int x[] = {0,5,10};
		int y[] = {0,20,20};
		
		shape = new Polygon(x, y, 3);
	    aft = new AffineTransform();
	    area = new Area(shape);
	}
}
